FROM mcantillana/django_unab
RUN apk add --no-cache jpeg-dev zlib-dev
ADD requirements.txt /code
RUN pip install -r requirements.txt